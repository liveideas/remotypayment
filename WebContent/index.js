var version = '2.0.0.5'
//TODO: Dev.
//var api_key = "ak_test_gOsHo1rYas2kqUP4zPn3DWPpn2ibiG";
//var encryption_key = "ek_test_1KT426zd5X2duwmYqWj8I3ZkkN9QbW";
//var postback_url = "http://localhost:8080/Remoty";
//var plans = [151439, 151440, 151441, 151445, 151443, 151444];

//TODO: Producao.
var api_key = "ak_live_K0SpFKU11qybWDuko5zzXMKQtNwW18";
var encryption_key = "ek_live_TmFRMoTBtp3PYVgZba7jhBxbxW933W";
var postback_url = "https://parceiros.remoty.com.br/Remoty/";
var plans	= [52673, 52677, 52679, 52676, 52678, 52680];

// A posicao 0 representa o plano e valor da licença 1.
// A posicao 1 representa o plano e valor da licença 2.

//Price Current
var valuePlans = [2990, 4990, 6990, 32292, 53892, 75492];

var idEquipment = 0;
var dataBuy;

//jQuery(document).ready(function($){
$(function() {
	$('#modalAlert').hide();
	//hide the subtle gradient layer (.pricing-list > li::after) when pricing table has been scrolled to the end (mobile version only)
	checkScrolling($('.pricing-body'));
	$(window).on('resize', function(){
		window.requestAnimationFrame(function(){checkScrolling($('.pricing-body'))});
	});
	$('.pricing-body').on('scroll', function(){ 
		var selected = $(this);
		window.requestAnimationFrame(function(){checkScrolling(selected)});
	});
	
	//switch from monthly to annual pricing tables
	bouncy_filter($('.pricing-container'));
	
	$("#popup_ok").click( function() {
		$('#modalAlert').hide();
	});
	
	$("#freeServices").load("freeServices.html", function(response, status, xhr) {
		if (status == "error") {
			var msg = "Sorry but there was an error: ";
			$("#error").html(msg + xhr.status + " " + xhr.statusText);
		}
		
		$("#btnSearchMacForFreeServices").click(function() {
			registerSubscription(dataBuy);
			$('#modal1').hide();
	       
		});
		
		$("#btnClose").click(function() {
			$('#modal1').hide();
//			messageAlert("Anteção", "Não foi possível salvar o seu pedido. Selecione a central para liberar serviços.")
		});

		$(".se-pre-con-page").fadeOut("slow");
	});
	
});

function checkScrolling(tables){
	tables.each(function(){
		var table= $(this),
			totalTableWidth = parseInt(table.children('.pricing-features').width()),
	 		tableViewport = parseInt(table.width());
		if( table.scrollLeft() >= totalTableWidth - tableViewport -1 ) {
			table.parent('li').addClass('is-ended');
		} else {
			table.parent('li').removeClass('is-ended');
		}
	});
}

function bouncy_filter(container) {
	container.each(function(){
		var pricing_table = $(this);
		var filter_list_container = pricing_table.children('.pricing-switcher'),
			filter_radios = filter_list_container.find('input[type="radio"]'),
			pricing_table_wrapper = pricing_table.find('.pricing-wrapper');

		//store pricing table items
		var table_elements = {};
		filter_radios.each(function(){
			var filter_type = $(this).val();
			table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="'+filter_type+'"]');
		});

		//detect input change event
		filter_radios.on('change', function(event){
			event.preventDefault();
			//detect which radio input item was checked
			var selected_filter = $(event.target).val();

			//give higher z-index to the pricing table items selected by the radio input
			show_selected_items(table_elements[selected_filter]);

			//rotate each pricing-wrapper 
			//at the end of the animation hide the not-selected pricing tables and rotate back the .pricing-wrapper
			
			if( !Modernizr.cssanimations ) {
				hide_not_selected_items(table_elements, selected_filter);
				pricing_table_wrapper.removeClass('is-switched');
			} else {
				pricing_table_wrapper.addClass('is-switched').eq(0).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {		
					hide_not_selected_items(table_elements, selected_filter);
					pricing_table_wrapper.removeClass('is-switched');
					//change rotation direction if .pricing-list has the .bounce-invert class
					if(pricing_table.find('.pricing-list').hasClass('bounce-invert')) pricing_table_wrapper.toggleClass('reverse-animation');
				});
			}
		});
	});
}
function show_selected_items(selected_elements) {
	selected_elements.addClass('is-selected');
}

function hide_not_selected_items(table_containers, filter) {
	$.each(table_containers, function(key, value){
  		if ( key != filter ) {	
			$(this).removeClass('is-visible is-selected').addClass('is-hidden');

		} else {	
			$(this).addClass('is-visible').removeClass('is-hidden is-selected');
		}
	});
}

function buySubscription(pos){
	
//	switch(valueMonth){
//		case "2990":
//			pos = 0;
//			break;
//		case "4990":
//			pos = 1;
//			break;
//		case "6990":
//			pos = 2;
//			break;
//		case "3391":
//			pos = 3;
//			break;
//		case "4241":
//			pos = 4;
//			break;
//		case "5941":
//			pos = 5;
//			break;
//	}
	
	idPlan = plans[parseInt(pos)];
	amount = valuePlans[parseInt(pos)];
	
    // INICIAR A INSTÂNCIA DO CHECKOUT
    // declarando um callback de sucesso
    var checkout = new PagarMeCheckout.Checkout({"encryption_key": encryption_key, 
    	success: function(data) {
            console.log(data);
            //Tratar aqui as ações de callback do checkout, como exibição de mensagem ou envio de token para captura da transação
            dataBuy = data;

            idEquipment = 0
			registerSubscription(dataBuy);
            
//            findEquipmentsByUser(data.customer.email);
    	},
    	error: function(data) {
        		alert(JSON.stringify("Failure : " + data));
//    		rConfirmV2("Atenção", "Houve um problema ao salvar seu pedido. Tente novamente mais tarde.", function() { });
			messageAlert("Anteção", "Houve um problema ao salvar seu pedido. Tente novamente mais tarde. 3");
    	}
    });

    // DEFINIR AS OPÇÕES
    // e abrir o modal
    // É necessário passar os valores boolean em "var params" como string
    var params = {"customerData":"true", "amount": amount, "createToken": "false"};
    checkout.open(params);
}

function messageAlert(title, message){
	$("#popup_title").text(title);
	$("#popup_message").text(message);
	$('#modalAlert').show();
}

function freeClientRemoty(email, name, idSubscription, idPlan) {

	$.ajax({
		type : "POST",
//		url : "http://localhost:8080/Remoty/freeRemotyClient",
		url : postback_url+"freeRemotyClient",
		data : JSON.stringify({
			"nmEmail" : email,
			"nmName" : name,
			"idSubscription" : idSubscription,
			"idPlan" : idPlan,
			"idEquipment" : idEquipment
		}),
		dataType : "json",
		contentType : "application/x-www-form-urlencoded;charset=ISO-8859-15",
		beforeSend : function(xhr) {
			xhr.setRequestHeader('Accept', "text/html; charset=ISO-8859-1");
		},
		success : function(data) {
		},
		failure : function(data) {
		}
	});
}

function findEquipmentsByUser(email) 
{
	
	$("#equipments").empty();
	var params = JSON.stringify({
					"nmEmail" : email
				 });
//	alert(params + "\n" + postback_url+"/getEquipmentsUser");
	$.ajax({
		type: "POST",
		url: postback_url+"getEquipmentsUser",
		data: params,
		dataType:"json",
		contentType: "application/x-www-form-urlencoded;charset=ISO-8859-15",
		beforeSend : function(xhr) {
            xhr.setRequestHeader('Accept', "text/html; charset=ISO-8859-1");
        },
		success: function(data){
			if(data.length == 1){
				idEquipment = data[0].idEquipment;
	            registerSubscription(dataBuy);
			}else if(data.length > 0){
				var i = 0;
				// alert(JSON.stringify(data));
				while (i < data.length) {
	//				alert("IdEquipment = " + data[i].idEquipment + " - NmEquipment = " + data[i].nmEquipment );
					$("#equipments").append("<option value='"+data[i].idEquipment+"'>"+ data[i].nmMacImei +"</option>");
					i++;
				} 
				
				idEquipment = data[0].idEquipment;
				$("#equipments option:selected").val(data[0].idEquipment);
				
				$('#equipments').on('change', function() {
					idEquipment = this.value;
				});
				
				$('#modal1').show();
			}else{
				idEquipment = 0
				registerSubscription(dataBuy);
			}
		},
		error: function(data) {
			alert(JSON.stringify(data));
			messageAlert("Atenção", "Houve um problema ao salvar seu pedido. Tente novamente mais tarde. 1");
		}
	});	 
}

function registerSubscription(data){

	if(data.payment_method == "credit_card"){
		var paramsSubscription = {
		  	"api_key" : api_key,
		  	"payment_method" : "credit_card",
		  	"postback_url" : postback_url + "/confirmStatusSubscriptionUser?nmEmail=" + data.customer.email + "&idEquipment=" + idEquipment,
		  	"plan_id" : idPlan,
		  	"card_hash" : data.card_hash,
		  	"customer" : data.customer,
	  	};
	}else{
		var paramsSubscription = {
			"api_key" : api_key,
	  	  	"payment_method" : "boleto",
	  	  	"postback_url" : postback_url + "/confirmStatusSubscriptionUser?nmEmail=" + data.customer.email + "&idEquipment=" + idEquipment,
	    	"plan_id" : idPlan,
	    	"customer" : data.customer,
		};
	}
	
	$.ajax({
		type : "POST",
		url : "https://api.pagar.me/1/subscriptions",
		data : paramsSubscription,
		dataType : "json",
		contentType : "application/x-www-form-urlencoded;charset=ISO-8859-15",
		beforeSend : function(xhr) {
			xhr.setRequestHeader('Accept', "text/html; charset=ISO-8859-1");
		},
		success : function(data) {
//			alert(JSON.stringify(data));
//			freeClientRemoty(data.customer.email, data.customer.name, data.id, data.plan.id);
//			rConfirmV2("Atenção", "Sua compra foi concluída com sucesso.", function() { });
			messageAlert("Atenção", "Sua compra foi concluída com sucesso.");
		},
		error : function(data) {
			alert(JSON.stringify("Erro : " + data));
			messageAlert("Atenção", "Houve um problema ao salvar seu pedido. Tente novamente mais tarde. 2");
		}
	});
}